﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PotentialsMethod
{
    public partial class Form1 : Form
    {
        private Graphics graphics;
        private PotentialsMethod method;
        private List<int> weigths;

        public Form1()
        {
            InitializeComponent();
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            try
            {
                var x11 = (int)class1X1.Value;
                var x12 = (int)class1X2.Value;
                var x21 = (int)class2X1.Value;
                var x22 = (int)class2X2.Value;
                var y11 = (int)class1Y1.Value;
                var y12 = (int)class1Y2.Value;
                var y21 = (int)class2Y1.Value;
                var y22 = (int)class2Y2.Value;

                if ((((x11 == x12 && x21 == x22) && ((y11 < y22 && y12 > y21) || (y12 < y22 && y12 > y21))) ||
                     ((y11 == y12 && y21 == y22) && ((x11 < x22 && x12 > x21) || (x12 < x22 && x12 > x21)))))
                {
                    MessageBox.Show("Incorrect input.");
                }
                else
                {
                    method = new PotentialsMethod();
                    var classes = new List<PotentialsMethod.Potential>();
                    classes.Add(new PotentialsMethod.Potential());
                    classes.Add(new PotentialsMethod.Potential());
                    classes[0].vectors.Add(new Point((int)class1X1.Value, (int)class1Y1.Value));
                    classes[0].vectors.Add(new Point((int)class1X2.Value, (int)class1Y2.Value));
                    classes[1].vectors.Add(new Point((int)class2X1.Value, (int)class2Y1.Value));
                    classes[1].vectors.Add(new Point((int)class2X2.Value, (int)class2Y2.Value));
                    bool correct = true;
                    if (correct)
                    {
                        method.SetObjects(classes);
                        weigths = method.CalculateFunction();
                        method.DrawChartGraphics(resultDraw);
                        string str;
                        str = weigths[0].ToString();
                        str += NumberToSignString(weigths[1]) + " * x1";
                        str += NumberToSignString(weigths[2]) + " * x2";
                        str += NumberToSignString(weigths[3]) + " * x1 * x2";
                        resultFunc.Text = str;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect input.");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private string NumberToSignString(int number)
        {
            if (number < 0)
            {
                return number.ToString();
            }
            else
            {
                return "+" + number.ToString();
            }
        }

        private void addNew_Click(object sender, EventArgs e)
        {
            if (method != null)
            {
                int res = method.DistributeVector(new Point((int)newX.Value, (int)newY.Value));
                if (res != 0)
                {
                    MessageBox.Show(String.Format("Object from {0} class.", res));
                }
                else
                {
                    MessageBox.Show(String.Format("Object on class border."));
                }
            }
        }
    }
}
