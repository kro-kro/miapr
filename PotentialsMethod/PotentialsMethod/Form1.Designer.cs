﻿namespace PotentialsMethod
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.class1X1 = new System.Windows.Forms.NumericUpDown();
            this.class1Y1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.class2Y1 = new System.Windows.Forms.NumericUpDown();
            this.class2X1 = new System.Windows.Forms.NumericUpDown();
            this.calculate = new System.Windows.Forms.Button();
            this.resultFunc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.resultDraw = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.newY = new System.Windows.Forms.NumericUpDown();
            this.newX = new System.Windows.Forms.NumericUpDown();
            this.addNew = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.class1Y2 = new System.Windows.Forms.NumericUpDown();
            this.class1X2 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.class2Y2 = new System.Windows.Forms.NumericUpDown();
            this.class2X2 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.class1X1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1Y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2Y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2X1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDraw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1Y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1X2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2Y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2X2)).BeginInit();
            this.SuspendLayout();
            // 
            // class1X1
            // 
            this.class1X1.Location = new System.Drawing.Point(25, 47);
            this.class1X1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.class1X1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class1X1.Name = "class1X1";
            this.class1X1.Size = new System.Drawing.Size(57, 20);
            this.class1X1.TabIndex = 0;
            this.class1X1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // class1Y1
            // 
            this.class1Y1.Location = new System.Drawing.Point(25, 69);
            this.class1Y1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.class1Y1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class1Y1.Name = "class1Y1";
            this.class1Y1.Size = new System.Drawing.Size(57, 20);
            this.class1Y1.TabIndex = 1;
            this.class1Y1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Class 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "X";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 237);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Y";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 214);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 169);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Class 2";
            // 
            // class2Y1
            // 
            this.class2Y1.Location = new System.Drawing.Point(25, 237);
            this.class2Y1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.class2Y1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class2Y1.Name = "class2Y1";
            this.class2Y1.Size = new System.Drawing.Size(57, 20);
            this.class2Y1.TabIndex = 6;
            // 
            // class2X1
            // 
            this.class2X1.Location = new System.Drawing.Point(25, 214);
            this.class2X1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.class2X1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class2X1.Name = "class2X1";
            this.class2X1.Size = new System.Drawing.Size(57, 20);
            this.class2X1.TabIndex = 5;
            this.class2X1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // calculate
            // 
            this.calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculate.Location = new System.Drawing.Point(6, 349);
            this.calculate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(76, 28);
            this.calculate.TabIndex = 10;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // resultFunc
            // 
            this.resultFunc.Location = new System.Drawing.Point(9, 408);
            this.resultFunc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.resultFunc.Name = "resultFunc";
            this.resultFunc.ReadOnly = true;
            this.resultFunc.Size = new System.Drawing.Size(74, 20);
            this.resultFunc.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 390);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Result func";
            // 
            // resultDraw
            // 
            chartArea2.Name = "ChartArea1";
            this.resultDraw.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.resultDraw.Legends.Add(legend2);
            this.resultDraw.Location = new System.Drawing.Point(92, 6);
            this.resultDraw.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.resultDraw.Name = "resultDraw";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.resultDraw.Series.Add(series2);
            this.resultDraw.Size = new System.Drawing.Size(850, 645);
            this.resultDraw.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 488);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Y";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 465);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "X";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 441);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 18);
            this.label10.TabIndex = 16;
            this.label10.Text = "New dot";
            // 
            // newY
            // 
            this.newY.Location = new System.Drawing.Point(25, 488);
            this.newY.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.newY.Name = "newY";
            this.newY.Size = new System.Drawing.Size(57, 20);
            this.newY.TabIndex = 15;
            // 
            // newX
            // 
            this.newX.Location = new System.Drawing.Point(25, 465);
            this.newX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.newX.Name = "newX";
            this.newX.Size = new System.Drawing.Size(57, 20);
            this.newX.TabIndex = 14;
            // 
            // addNew
            // 
            this.addNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNew.Location = new System.Drawing.Point(5, 518);
            this.addNew.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addNew.Name = "addNew";
            this.addNew.Size = new System.Drawing.Size(76, 28);
            this.addNew.TabIndex = 19;
            this.addNew.Text = "Add";
            this.addNew.UseVisualStyleBackColor = true;
            this.addNew.Click += new System.EventHandler(this.addNew_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(22, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "Object 1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 138);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 17);
            this.label12.TabIndex = 24;
            this.label12.Text = "Y";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 116);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 17);
            this.label13.TabIndex = 23;
            this.label13.Text = "X";
            // 
            // class1Y2
            // 
            this.class1Y2.Location = new System.Drawing.Point(25, 138);
            this.class1Y2.Margin = new System.Windows.Forms.Padding(2);
            this.class1Y2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class1Y2.Name = "class1Y2";
            this.class1Y2.Size = new System.Drawing.Size(57, 20);
            this.class1Y2.TabIndex = 22;
            // 
            // class1X2
            // 
            this.class1X2.Location = new System.Drawing.Point(25, 116);
            this.class1X2.Margin = new System.Windows.Forms.Padding(2);
            this.class1X2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class1X2.Name = "class1X2";
            this.class1X2.Size = new System.Drawing.Size(57, 20);
            this.class1X2.TabIndex = 21;
            this.class1X2.Value = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 99);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 15);
            this.label14.TabIndex = 25;
            this.label14.Text = "Object 2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 268);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 15);
            this.label15.TabIndex = 27;
            this.label15.Text = "Object 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 197);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 15);
            this.label16.TabIndex = 26;
            this.label16.Text = "Object 1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 308);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 17);
            this.label17.TabIndex = 31;
            this.label17.Text = "Y";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 285);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 17);
            this.label18.TabIndex = 30;
            this.label18.Text = "X";
            // 
            // class2Y2
            // 
            this.class2Y2.Location = new System.Drawing.Point(25, 308);
            this.class2Y2.Margin = new System.Windows.Forms.Padding(2);
            this.class2Y2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class2Y2.Name = "class2Y2";
            this.class2Y2.Size = new System.Drawing.Size(57, 20);
            this.class2Y2.TabIndex = 29;
            this.class2Y2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // class2X2
            // 
            this.class2X2.Location = new System.Drawing.Point(25, 285);
            this.class2X2.Margin = new System.Windows.Forms.Padding(2);
            this.class2X2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.class2X2.Name = "class2X2";
            this.class2X2.Size = new System.Drawing.Size(57, 20);
            this.class2X2.TabIndex = 28;
            this.class2X2.Value = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 662);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.class2Y2);
            this.Controls.Add(this.class2X2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.class1Y2);
            this.Controls.Add(this.class1X2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.addNew);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.newY);
            this.Controls.Add(this.newX);
            this.Controls.Add(this.resultDraw);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.resultFunc);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.class2Y1);
            this.Controls.Add(this.class2X1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.class1Y1);
            this.Controls.Add(this.class1X1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.class1X1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1Y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2Y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2X1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDraw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1Y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1X2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2Y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class2X2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown class1X1;
        private System.Windows.Forms.NumericUpDown class1Y1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown class2Y1;
        private System.Windows.Forms.NumericUpDown class2X1;
        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.TextBox resultFunc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataVisualization.Charting.Chart resultDraw;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown newY;
        private System.Windows.Forms.NumericUpDown newX;
        private System.Windows.Forms.Button addNew;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown class1Y2;
        private System.Windows.Forms.NumericUpDown class1X2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown class2Y2;
        private System.Windows.Forms.NumericUpDown class2X2;
    }
}

