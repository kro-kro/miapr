﻿namespace FirstK
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picture = new System.Windows.Forms.PictureBox();
            this.imageCount = new System.Windows.Forms.NumericUpDown();
            this.classCount = new System.Windows.Forms.NumericUpDown();
            this.calculate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classCount)).BeginInit();
            this.SuspendLayout();
            // 
            // picture
            // 
            this.picture.Location = new System.Drawing.Point(95, -1);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(1016, 680);
            this.picture.TabIndex = 0;
            this.picture.TabStop = false;
            // 
            // imageCount
            // 
            this.imageCount.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.imageCount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.imageCount.Location = new System.Drawing.Point(1, 179);
            this.imageCount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.imageCount.Name = "imageCount";
            this.imageCount.Size = new System.Drawing.Size(94, 22);
            this.imageCount.TabIndex = 1;
            this.imageCount.Tag = "";
            // 
            // classCount
            // 
            this.classCount.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.classCount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.classCount.Location = new System.Drawing.Point(1, 207);
            this.classCount.Name = "classCount";
            this.classCount.Size = new System.Drawing.Size(94, 22);
            this.classCount.TabIndex = 2;
            // 
            // calculate
            // 
            this.calculate.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.calculate.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.calculate.Location = new System.Drawing.Point(1, 235);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(94, 23);
            this.calculate.TabIndex = 3;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = false;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1109, 678);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.classCount);
            this.Controls.Add(this.imageCount);
            this.Controls.Add(this.picture);
            this.ForeColor = System.Drawing.Color.MistyRose;
            this.Name = "Form1";
            this.Text = "K-middle";
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.NumericUpDown imageCount;
        private System.Windows.Forms.NumericUpDown classCount;
        private System.Windows.Forms.Button calculate;
    }
}

