﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstK
{
    internal class KMid
    {
        private List<Color> colorList = new List<Color>();
        private int classCount, imageCount;
        private List<Dot> imageCollection = new List<Dot>();
        private List<Dot> centerCollection = new List<Dot>();
        Random rnd = new Random();

        private int maxX;
        private int maxY;

        public KMid(int classCount, int imageCount, int maxX, int maxY)
        {
            this.classCount = classCount;
            this.imageCount = imageCount;
            this.maxX = Math.Abs(maxX);
            this.maxY = Math.Abs(maxY);

            for (int i = 0; i < classCount; i++)
            {
                colorList.Add(Color.FromArgb((byte)rnd.Next(Byte.MaxValue), (byte)rnd.Next(Byte.MaxValue), (byte)rnd.Next(Byte.MaxValue)));
            }
        }

        public int Calculate(Graphics result)
        {
            int iteration = 1;
            GenerateVectors(imageCount);
            CreateCenterCollection(classCount);
            Recalculate();
            Draw(result);
            while (NeedRecalculation())
            {
                Recalculate();
                iteration++;
                Draw(result);
            }
            return iteration;
        }

        private void GenerateVectors(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var img = new Image(new Point(rnd.Next(0, maxX), rnd.Next(0, maxY)));
                imageCollection.Add(img);
            }
        }

        private void CreateCenterCollection(int count)
        {
            int i = 0;
            while (i < count)
            {
                Image image;
                ClassInfo center;
                image = (Image)imageCollection[rnd.Next(imageCount)];
                if (centerCollection.Where(x => x.DotPoint == image.DotPoint).Count() == 0)
                {
                    center = new ClassInfo(image.DotPoint, colorList[i]);
                    centerCollection.Add(center);
                    i++;
                }
            }
        }

        private bool NeedRecalculation()
        {
            bool result = false;
            Parallel.ForEach(centerCollection, (center, state) =>
            {
                if (((ClassInfo)center).ChangeCenter())
                {
                    result = true;
                    state.Break();
                }
            });
            return result;
        }

        private void Recalculate()
        {
            Parallel.ForEach(imageCollection, img =>
            {
                Image image = (Image)img;
                foreach (ClassInfo center in centerCollection)
                {
                    if (image.Parent == null)
                    {
                        image.Parent = center;
                    }
                    else
                    {
                        if (image.CountDistance(image.DotPoint, center.DotPoint) < image.CountDistance(image.DotPoint, image.Parent.DotPoint))
                        {
                            image.Parent = center;
                        }
                    }
                }
            });
            foreach (ClassInfo center in centerCollection)
            {
                center.ClearRelatedImages();
            }
            foreach (Image image in imageCollection)
            {
                foreach (ClassInfo center in centerCollection)
                {
                    if (image.Parent.Equals(center))
                    {
                        center.AddImage(image);
                        break;
                    }
                }
            }
        }

        private void Draw(Graphics result)
        {
            IntPtr hdc = result.GetHdc();
            foreach (Image image in imageCollection)
            {
                image.Draw(hdc);
            }
            foreach (ClassInfo center in centerCollection)
            {
                center.Draw(hdc);
            }
            result.ReleaseHdc();
        }
    }
}
