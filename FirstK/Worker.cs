﻿using System;
using System.Diagnostics;
using System.Drawing;

namespace FirstK
{
    internal class Worker
    {
        private Tuple<TimeSpan, int> result;
        KMid kMid;

        public Worker(KMid kMid)
        {
            this.kMid = kMid;
        }

        public void Start(Graphics resultDraw)
        {
            resultDraw.Clear(Color.Black);
            Stopwatch timer = new Stopwatch();
            timer.Start();

            //start here
            int iterations = kMid.Calculate(resultDraw);

            //end
            timer.Stop();
            result = new Tuple<TimeSpan, int>(timer.Elapsed, iterations);
            resultDraw.Dispose();
        }

        public string GetResult()
        {
            return $"Time calculating: {result.Item1.Minutes}:{result.Item1.Seconds}:{result.Item1.Milliseconds}\nResult on iteration #{result.Item2}.";
        }
    }
}
