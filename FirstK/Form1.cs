﻿using System;
using System.Windows.Forms;
namespace FirstK
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            //prepearing
            KMid kMid = new KMid((int)classCount.Value, (int)imageCount.Value, picture.Width, picture.Height);
            Worker worker = new Worker(kMid);

            //start here
            worker.Start(picture.CreateGraphics());

            //show result
            if (MessageBox.Show(worker.GetResult(), "Done!", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
            {
                Close();
            }
        }
    }
}
