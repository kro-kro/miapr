﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace FirstK
{
    internal class ClassInfo : Dot
    {
        private List<Image> relatedImageList = new List<Image>();

        public ClassInfo(Point centerPoint, Color relatedColor)
        {
            DotPoint = centerPoint;
            RelatedColor = relatedColor;
        }

        public void AddImage(Image image)
        {
            relatedImageList.Add(image);
        }

        public void RemoveImage(Image image)
        {
            relatedImageList.Remove(image);
        }

        public void ClearRelatedImages()
        {
            relatedImageList.Clear();
        }

        public bool ChangeCenter()
        {
            bool changed = false;
            Point averagePoint = Average();
            //if (averagePoint == DotPoint) return false;
            Parallel.ForEach(relatedImageList, (image, state) =>
            {
                if (CountDistance(image.DotPoint, averagePoint) < CountDistance(DotPoint, averagePoint))
                {
                    changed = true;
                    DotPoint = image.DotPoint;
                }
            });
            return changed;
        }

        private Point Average()
        {
            if (relatedImageList.Count > 0)
            {
                double sumX = 0, sumY = 0;
                foreach (Image img in relatedImageList)
                {
                    sumX += img.DotPoint.X;
                    sumY += img.DotPoint.Y;
                }
                return new Point((int)(sumX / relatedImageList.Count), (int)(sumY / relatedImageList.Count));
            }
            return DotPoint;
        }
    }
}
