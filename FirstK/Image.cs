﻿using System.Drawing;

namespace FirstK
{
    internal class Image : Dot
    {
        private ClassInfo parent;
        public ClassInfo Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                RelatedColor = parent.RelatedColor;
            }
        }

        public Image(Point imagePoint)
        {
            DotPoint = imagePoint;
        }
    }
}
