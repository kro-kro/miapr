﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Graph
{
    public partial class Form1 : Form
    {
        private int pointsCount = 1000;
        private double pc1;
        private double pc2;
        private Random random;

        public Form1()
        {
            InitializeComponent();
        }

        private void c1_Leave(object sender, EventArgs e)
        {
            c2.Value = 1 - c1.Value;
            pc1 = (double)c1.Value;
            pc2 = (double)c2.Value;
        }

        private void c2_Leave(object sender, EventArgs e)
        {
            c1.Value = 1 - c2.Value;
            pc1 = (double)c1.Value;
            pc2 = (double)c2.Value;
        }

        private void pointCount_ValueChanged(object sender, EventArgs e)
        {
            pointsCount = (int)((NumericUpDown)sender).Value;
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            random = new Random();
            var bmp = new Bitmap(resultPict.Width, resultPict.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                Calculate(g);
                resultPict.Image = bmp;
            }
        }

        private void Calculate(Graphics g)
        {
            int[] group1 = new int[pointsCount], group2 = new int[pointsCount];
            double midGroup1 = GeneratePointCollection(group1, 100, 740);
            double midGroup2 = GeneratePointCollection(group2, -100, 540);

            double sigmaGr1 = CalculateSigma(group1, midGroup1);
            double sigmaGr2 = CalculateSigma(group2, midGroup2);

            double[] resultGr1 = new double[resultPict.Width];
            double[] resultGr2 = new double[resultPict.Width];
            int d = CalculateResult(g, resultGr1, resultGr2, midGroup1, midGroup2, sigmaGr1, sigmaGr2);

            double err1 = resultGr2.Take(d).Sum();
            double err2;

            if (pc1 > pc2)
            {
                err2 = resultGr2.Skip(d).Sum();
            }
            else
            {
                err2 = resultGr1.Skip(d).Sum();
            }

            DrawTemplate(g, d);
            SetResult(err1, err2);
        }

        private double GeneratePointCollection(int[] group, int randomMin, int randomMax)
        {
            int midGroup = 0;
            for (int i = 0; i < pointsCount; i++)
            {
                group[i] = random.Next(randomMin, randomMax);
                midGroup += group[i];
            }
            return midGroup /= pointsCount;
        }

        private double CalculateSigma(int[] group, double midGr)
        {
            double sigma = 0;
            for (int i = 0; i < pointsCount; i++)
            {
                sigma += Math.Pow(group[i] - midGr, 2);
            }
            return Math.Sqrt(sigma / pointsCount);
        }

        private int CalculateResult(Graphics g, double[] result1, double[] result2, double midGr1, double midGr2, double sigma1, double sigma2)
        {
            result1[0] = (Math.Exp(-0.5 * Math.Pow((-100 - midGr1) / sigma1, 2)) / (sigma1 * Math.Sqrt(2 * Math.PI)) * pc1);
            result2[0] = (Math.Exp(-0.5 * Math.Pow((-100 - midGr2) / sigma2, 2)) / (sigma2 * Math.Sqrt(2 * Math.PI)) * pc2);

            int d = 0;
            for (int x = 1; x < resultPict.Width; x++)
            {
                result1[x] = (Math.Exp(-0.5 * Math.Pow((x - 100 - midGr1) / sigma1, 2)) / (sigma1 * Math.Sqrt(2 * Math.PI)) * pc1);
                result2[x] = (Math.Exp(-0.5 * Math.Pow((x - 100 - midGr2) / sigma2, 2)) / (sigma2 * Math.Sqrt(2 * Math.PI)) * pc2);

                if (Math.Abs(result1[x] * 500 - result2[x] * 500) < 0.002)
                {
                    d = x;
                }

                g.DrawLine(Pens.Blue, new Point(x - 1, (resultPict.Height - (int)(result1[x - 1] * resultPict.Height * 500))), new Point(x, (resultPict.Height - (int)(result1[x] * resultPict.Height * 500))));
                g.DrawLine(Pens.Red, new Point(x - 1, (resultPict.Height - (int)(result2[x - 1] * resultPict.Height * 500))), new Point(x, (resultPict.Height - (int)(result2[x] * resultPict.Height * 500))));
            }
            return d;
        }

        private void DrawTemplate(Graphics g, int d)
        {
            SolidBrush brush = new SolidBrush(Color.Black);
            
            g.DrawLine(Pens.Chartreuse, d, 0, d, resultPict.Height);
            g.DrawLine(Pens.Black, 0, resultPict.Height - 1, resultPict.Width, resultPict.Height - 1);
            g.DrawLine(Pens.Black, resultPict.Width, resultPict.Height - 1, resultPict.Width - 15, resultPict.Height - 5);
            g.DrawLine(Pens.Black, 100, resultPict.Height - 1, 100, 0);
            g.DrawLine(Pens.Black, 100, 0, 95, 15);
            g.DrawLine(Pens.Black, 100, 0, 105, 15);
            g.DrawString("X", Font, Brushes.Black, resultPict.Width - 10, resultPict.Height - 20);
            g.DrawLine(Pens.Blue, resultPict.Width - 150, 15, resultPict.Width - 100, 15);
            g.DrawString("p(X / C1) P(C1)", Font, brush, resultPict.Width - 90, 5);

            g.DrawLine(Pens.Red, resultPict.Width - 150, 30, resultPict.Width - 100, 30);
            g.DrawString("p(X / C2) P(C2)", Font, brush, resultPict.Width - 90, 25);

            brush.Dispose();
        }

        private void SetResult(double err1, double err2)
        {
            falseAlert.Text = err1.ToString();
            missedDetection.Text = err2.ToString();
            classificationError.Text = (err1 + err2).ToString();
        }
    }
}
