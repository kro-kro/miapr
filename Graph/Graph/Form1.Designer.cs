﻿namespace Graph
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultPict = new System.Windows.Forms.PictureBox();
            this.c1 = new System.Windows.Forms.NumericUpDown();
            this.c2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.falseAlert = new System.Windows.Forms.TextBox();
            this.classificationError = new System.Windows.Forms.TextBox();
            this.missedDetection = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.calculate = new System.Windows.Forms.Button();
            this.pointCount = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.resultPict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointCount)).BeginInit();
            this.SuspendLayout();
            // 
            // resultPict
            // 
            this.resultPict.Location = new System.Drawing.Point(132, -3);
            this.resultPict.Name = "resultPict";
            this.resultPict.Size = new System.Drawing.Size(1314, 710);
            this.resultPict.TabIndex = 0;
            this.resultPict.TabStop = false;
            // 
            // c1
            // 
            this.c1.DecimalPlaces = 2;
            this.c1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.c1.Location = new System.Drawing.Point(1, 172);
            this.c1.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(132, 22);
            this.c1.TabIndex = 1;
            this.c1.Leave += new System.EventHandler(this.c1_Leave);
            // 
            // c2
            // 
            this.c2.DecimalPlaces = 2;
            this.c2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.c2.Location = new System.Drawing.Point(1, 217);
            this.c2.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(132, 22);
            this.c2.TabIndex = 2;
            this.c2.Leave += new System.EventHandler(this.c2_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "p(C1)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "p(C2)";
            // 
            // falseAlert
            // 
            this.falseAlert.Location = new System.Drawing.Point(1, 391);
            this.falseAlert.MaxLength = 5;
            this.falseAlert.Name = "falseAlert";
            this.falseAlert.ReadOnly = true;
            this.falseAlert.Size = new System.Drawing.Size(132, 22);
            this.falseAlert.TabIndex = 5;
            // 
            // classificationError
            // 
            this.classificationError.Location = new System.Drawing.Point(1, 481);
            this.classificationError.MaxLength = 5;
            this.classificationError.Name = "classificationError";
            this.classificationError.ReadOnly = true;
            this.classificationError.Size = new System.Drawing.Size(132, 22);
            this.classificationError.TabIndex = 6;
            // 
            // missedDetection
            // 
            this.missedDetection.Location = new System.Drawing.Point(1, 436);
            this.missedDetection.MaxLength = 5;
            this.missedDetection.Name = "missedDetection";
            this.missedDetection.ReadOnly = true;
            this.missedDetection.Size = new System.Drawing.Size(132, 22);
            this.missedDetection.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "false alert";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 416);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "missed detection";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 461);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "class-tion error";
            // 
            // calculate
            // 
            this.calculate.Location = new System.Drawing.Point(1, 313);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(132, 36);
            this.calculate.TabIndex = 11;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // pointCount
            // 
            this.pointCount.Location = new System.Drawing.Point(1, 262);
            this.pointCount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.pointCount.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pointCount.Name = "pointCount";
            this.pointCount.Size = new System.Drawing.Size(132, 22);
            this.pointCount.TabIndex = 12;
            this.pointCount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.pointCount.ValueChanged += new System.EventHandler(this.pointCount_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Point count";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1446, 708);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pointCount);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.missedDetection);
            this.Controls.Add(this.classificationError);
            this.Controls.Add(this.falseAlert);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c2);
            this.Controls.Add(this.c1);
            this.Controls.Add(this.resultPict);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.resultPict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox resultPict;
        private System.Windows.Forms.NumericUpDown c1;
        private System.Windows.Forms.NumericUpDown c2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox falseAlert;
        private System.Windows.Forms.TextBox classificationError;
        private System.Windows.Forms.TextBox missedDetection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.NumericUpDown pointCount;
        private System.Windows.Forms.Label label6;
    }
}

