﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace MaxiMin
{
    internal class Dot
    {
        protected internal Point DotPoint { get; set; }
        internal Color RelatedColor { get; set; }

        protected internal double CountDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        [DllImport("gdi32.dll")]
        private static extern uint SetPixel(IntPtr hdc, int X, int Y, uint crColor);
        private static uint ColorToUInt(Color color)
        {
            return (((color.R | ((uint)(color.G) << 8)) | (((uint)(color.B)) << 16)));
        }
        public void Draw(IntPtr hdc)
        {
            SetPixel(hdc, DotPoint.X, DotPoint.Y, ColorToUInt(RelatedColor));
        }
    }
}
