﻿using System.Drawing;

namespace MaxiMin
{
    internal class Image : Dot
    {
        private ImageClass parent;
        public ImageClass Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                RelatedColor = parent.RelatedColor;
            }
        }

        public Image(Point imagePoint)
        {
            DotPoint = imagePoint;
        }
    }
}
