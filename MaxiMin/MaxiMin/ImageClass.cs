﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace MaxiMin
{
    internal class ImageClass : Dot
    {
        private List<Image> relatedImageList = new List<Image>();

        public ImageClass(Point centerPoint, Color relatedColor)
        {
            DotPoint = centerPoint;
            RelatedColor = relatedColor;
        }

        public void AddImage(Image image)
        {
            relatedImageList.Add(image);
        }

        public void RemoveImage(Image image)
        {
            relatedImageList.Remove(image);
        }

        public void ClearRelatedImages()
        {
            relatedImageList.Clear();
        }

        public Image GetFarestImage()
        {
            Image farest = new Image(DotPoint);
            Parallel.ForEach(relatedImageList, (image, state) =>
            {
                if (CountDistance(image.DotPoint, DotPoint) > CountDistance(farest.DotPoint, DotPoint))
                {
                    farest = image;
                }
            });
            return farest;
        }
    }
}
