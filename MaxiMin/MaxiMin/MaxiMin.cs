﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace MaxiMin
{
    internal class MaxiMin
    {
        private List<Color> colorList = new List<Color>();
        private List<Dot> imageCollection = new List<Dot>();
        private List<Dot> centerCollection = new List<Dot>();
        private int imageCount;
        private int maxX;
        private int maxY;
        private Random rnd = new Random();
        private Graphics g;
        private bool done = false;

        public MaxiMin(int imageCount, int maxX, int maxY)
        {
            this.imageCount = imageCount;
            this.maxX = Math.Abs(maxX);
            this.maxY = Math.Abs(maxY);
        }

        private void AddColor()
        {
            colorList.Add(Color.FromArgb((byte)rnd.Next(Byte.MaxValue), (byte)rnd.Next(Byte.MaxValue), (byte)rnd.Next(Byte.MaxValue)));
        }

        public int Calculate(Graphics result)
        {
            int iteration = 1;
            GenerateVectors(imageCount);
            AddColor();
            CreateFirstCenter();
            Recalculate();
            Draw(result);
            while (NeedRecalculation(CalculateNewCenter()))
            {
                Recalculate();
                iteration++;
                Draw(result);
            }
            return iteration;
        }

        private void GenerateVectors(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var img = new Image(new Point(rnd.Next(0, maxX), rnd.Next(0, maxY)));
                imageCollection.Add(img);
            }
        }

        private void CreateFirstCenter()
        {
            int i = 0;
            Image image = (Image)imageCollection[rnd.Next(imageCount)];
            ImageClass center = new ImageClass(image.DotPoint, colorList[i]);
            centerCollection.Add(center);
            foreach (Image img in imageCollection)
            {
                center.AddImage(img);
                img.Parent = center;
            }
        }

        private void CreateNewCenter(Image image)
        {
            AddColor();
            ImageClass center = new ImageClass(image.DotPoint, colorList.Last());
        }

        private bool NeedRecalculation(Double distance)
        {
            double[] toCompare = new double[centerCollection.Count()];
            Parallel.ForEach(centerCollection, (center, state) =>
            {
                for (int i = 0; i < centerCollection.Count(); i++)
                {
                    toCompare[i] = center.CountDistance(center.DotPoint, centerCollection[i].DotPoint);
                }
            });
            if (toCompare.Sum() / toCompare.Count() / 2 < distance) return true;
            return false;
        }

        private double CalculateNewCenter()
        {
            Image[] far = new Image[centerCollection.Count()];
            Parallel.For(0, centerCollection.Count(), i =>
            {
                far[i] = ((ImageClass)centerCollection[i]).GetFarestImage();
            });
            double[] result = new double[centerCollection.Count()];
            Array.Clear(result, 0, result.Length);
            for (int c = 0; c < centerCollection.Count(); c++)
            {
                ImageClass center = (ImageClass)centerCollection[c];
                for (int i = 0; i < far.Count(); i++)
                {
                    result[i] += center.CountDistance(center.DotPoint, far[i].DotPoint);
                }
            }
            Parallel.For(0, result.Count(), i => { result[i] /= centerCollection.Count(); });
            //new center here
            return 0;
        }

        private void Recalculate()
        {
            foreach (ImageClass center in centerCollection)
            {
                center.ClearRelatedImages();
            }
            foreach (Image image in imageCollection)
            {
                foreach (ImageClass center in centerCollection)
                {
                    if (image.Parent.Equals(center))
                    {
                        center.AddImage(image);
                        break;
                    }
                }
            }
        }

        private void Draw(Graphics result)
        {
            IntPtr hdc = result.GetHdc();
            foreach (Image image in imageCollection)
            {
                image.Draw(hdc);
            }
            foreach (ImageClass center in centerCollection)
            {
                center.Draw(hdc);
            }
            result.ReleaseHdc();
        }
    }
}
