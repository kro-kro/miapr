﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MaxiMin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            //prepearing
            MaxiMin maxiMin = new MaxiMin((int)imageCount.Value, picture.Width, picture.Height);
            Worker worker = new Worker(maxiMin);

            //start here
            worker.Start(picture.CreateGraphics());

            //show result
            if (MessageBox.Show(worker.GetResult(), "Done!", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
            {
                Close();
            }
        }
    }
}
