﻿namespace MaxiMin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculate = new System.Windows.Forms.Button();
            this.imageCount = new System.Windows.Forms.NumericUpDown();
            this.picture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            this.SuspendLayout();
            // 
            // calculate
            // 
            this.calculate.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.calculate.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.calculate.Location = new System.Drawing.Point(1, 266);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(94, 23);
            this.calculate.TabIndex = 7;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = false;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // imageCount
            // 
            this.imageCount.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.imageCount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.imageCount.Location = new System.Drawing.Point(1, 238);
            this.imageCount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.imageCount.Name = "imageCount";
            this.imageCount.Size = new System.Drawing.Size(94, 22);
            this.imageCount.TabIndex = 5;
            this.imageCount.Tag = "";
            // 
            // picture
            // 
            this.picture.Location = new System.Drawing.Point(92, 1);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(1016, 680);
            this.picture.TabIndex = 4;
            this.picture.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1109, 681);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.imageCount);
            this.Controls.Add(this.picture);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.imageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.NumericUpDown imageCount;
        private System.Windows.Forms.PictureBox picture;
    }
}

