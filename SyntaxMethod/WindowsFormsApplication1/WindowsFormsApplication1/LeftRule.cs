﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace WindowsFormsApplication1
{
    public class LeftRule : Rule
    {
        private const int randomDelta = 3;

        public LeftRule(SType startElementType, SType firstArgumentType,
            SType secondArgumentType) : base(startElementType, firstArgumentType, secondArgumentType) { }

        public override SObjects TransformConnect(SObjects first, SObjects second)
        {
            second.Shift(first.Length + Random.Next(1, 10), 0);
            return Connect(first, second);
        }

        public override SObjects Connect(SObjects first, SObjects second)
        {
            var resultLines = new List<SLine>(first.Lines);

            resultLines.AddRange(second.Lines);

            var startPosition = new Point(first.StartPosition.X,
                Math.Max(first.StartPosition.Y, second.StartPosition.Y));
            var endPosition = new Point(second.EndPosition.X,
                Math.Min(first.EndPosition.Y, second.EndPosition.Y));
            var connect = new SObjects(StartElementType, resultLines, startPosition, endPosition);

            return connect;
        }

        public override bool IsRulePare(SObjects first, SObjects second)
        {
            if (first.Type.Name != FirstArgumentType.Name ||
                second.Type.Name != SecondArgumentType.Name)
                return false;

            return first.EndPosition.X - randomDelta < second.StartPosition.X;
        }
    }
}
