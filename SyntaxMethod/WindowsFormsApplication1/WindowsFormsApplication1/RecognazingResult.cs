﻿using System;

namespace WindowsFormsApplication1
{
    public class RecognazingResult
    {
        public RecognazingResult(string errorElementName, bool isHome)
        {
            ErrorElementName = errorElementName;
            IsHome = isHome;
        }
        public String ErrorElementName { get; set; }
        public bool IsHome { get; set; }
    }
}
