﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using Point = System.Windows.Point;

namespace WindowsFormsApplication1
{
    public class SObjects
    {
        public SObjects(SType type)
        {
            Type = type;
            Lines = new List<SLine>();
        }
        public SObjects(SType type, SLine line)
        {
            Type = type;
            Lines = new List<SLine> { line };
            StartPosition = new Point(Math.Min(line.Start.X, line.Finish.X),
                Math.Max(line.Start.Y, line.Finish.Y));
            EndPosition = new Point(Math.Max(line.Start.X, line.Finish.X),
                Math.Min(line.Start.Y, line.Finish.Y));
        }
        public SObjects(SType type, IEnumerable<SLine> lines, Point startPoint, Point endPoint)
        {
            StartPosition = startPoint;
            EndPosition = endPoint;
            Type = type;
            Lines = lines;
        }

        public SType Type { get; private set; }
        public Point StartPosition { get; set; }
        public Point EndPosition { get; set; }
        public IEnumerable<SLine> Lines { get; private set; }

        public double Length
        {
            get { return Math.Abs(EndPosition.X - StartPosition.X); }
        }

        public double Height
        {
            get { return Math.Abs(EndPosition.Y - StartPosition.Y); }
        }

        public void Scale(double xScale, double yScale)
        {
            Vector delta = EndPosition - StartPosition;

            delta.X *= xScale;
            delta.Y *= yScale;
            EndPosition = StartPosition + delta;

            foreach (SLine line in Lines)
                line.Scale(xScale, yScale, StartPosition);
        }
        public void Shift(double xDelta, double yDelta)
        {
            var shift = new Vector(xDelta, yDelta);

            StartPosition += shift;
            EndPosition += shift;

            foreach (SLine line in Lines)
                line.Shift(xDelta, yDelta);
        }

        public void Draw(Graphics g)
        {
            var pen = new Pen(Color.Black);

            foreach (SLine line in Lines)
            {
                var from = new System.Drawing.Point(Convert.ToInt32(GetScreenPoint(line.Start).X), Convert.ToInt32(GetScreenPoint(line.Start).Y));
                var to = new System.Drawing.Point(Convert.ToInt32(GetScreenPoint(line.Finish).X), Convert.ToInt32(GetScreenPoint(line.Finish).Y));
                g.DrawLine(pen, from, to);
            }
        }

        private Point GetScreenPoint(Point point)
        {
            return new Point(point.X, StartPosition.Y - point.Y);
        }
    }
}
