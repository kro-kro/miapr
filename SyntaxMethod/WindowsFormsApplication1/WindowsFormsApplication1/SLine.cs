﻿using System.Windows;

namespace WindowsFormsApplication1
{
    public class SLine
    {
        public SLine(Point start, Point finish)
        {
            Start = start;
            Finish = finish;
        }

        public Point Start { get; set; }
        public Point Finish { get; set; }

        public void Scale(double xScale, double yScale, Point centerPoint)
        {
            Vector length = Finish - Start;
            Vector startDelta = Start - centerPoint;

            startDelta.X *= xScale;
            startDelta.Y *= yScale;
            Start = centerPoint + startDelta;
            length.X *= xScale;
            length.Y *= yScale;
            Finish = Start + length;
        }
        public void Shift(double xDelta, double yDelta)
        {
            var shift = new Vector(xDelta, yDelta);

            Finish += shift;
            Start += shift;
        }
    }
}
