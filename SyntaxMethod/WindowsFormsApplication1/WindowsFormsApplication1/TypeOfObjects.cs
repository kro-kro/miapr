﻿using System.Windows;

namespace WindowsFormsApplication1
{
    internal class TypeOfObjects : SType
    {
        private readonly SLine standartElementLine;

        public TypeOfObjects(string name, SLine standartElementLine) : base(name)
        {
            this.standartElementLine = standartElementLine;
        }

        public SObjects Default
        {
            get
            {
                return new SObjects(this, new SLine(new Point(standartElementLine.Start.X, standartElementLine.Start.Y), new Point(standartElementLine.Finish.X, standartElementLine.Finish.Y)));
            }
        }
    }
}
