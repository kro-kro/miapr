﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Point = System.Windows.Point;

namespace WindowsFormsApplication1
{
    public class Syntax
    {
        private static readonly Dictionary<string, SType> ElementTypes = GetElementTypes();
        private readonly List<Rule> rules;
        private readonly SType startElementType;

        public Syntax()
        {
            startElementType = new SType("face");
            rules = new List<Rule>
            {
                new LeftRule(ElementTypes["ear"], ElementTypes["a3"], ElementTypes["a4"]),
                new LeftRule(ElementTypes["ears"], ElementTypes["ear"], ElementTypes["ear"]),
                new LeftRule(ElementTypes["parallel"], ElementTypes["a4"], ElementTypes["a3"]),
                new UpRule(ElementTypes["square"], ElementTypes["ear"], ElementTypes["parallel"]),
                new UpRule(ElementTypes["eye"], ElementTypes["ear"], ElementTypes["a1"]),
                new LeftRule(ElementTypes["eyes"], ElementTypes["eye"], ElementTypes["eye"]),
                new UpRule(ElementTypes["eyesAndMouth"], ElementTypes["eyes"], ElementTypes["a1"]),
                new InsideRule(ElementTypes["head"], ElementTypes["eyesAndMouth"],ElementTypes["square"] ),
                new UpRule(startElementType, ElementTypes["ears"], ElementTypes["head"]),
            };
        }

        private static Dictionary<string, SType> GetElementTypes()
        {
            return new Dictionary<string, SType>
            {
                {"a1", new TypeOfObjects("a1", new SLine(new Point(0, 0), new Point(10, 0)))},
                {"a2", new TypeOfObjects("a2", new SLine(new Point(0, 0), new Point(0, 10)))},
                {"a3", new TypeOfObjects("a3", new SLine(new Point(0, 0), new Point(10, 10)))},
                {"a4", new TypeOfObjects("a4", new SLine(new Point(10, 0), new Point(0, 10)))},
                {"ear", new SType("ear")},
                {"ears", new SType("ears")},
                {"parallel", new SType("parallel")},
                {"square", new SType("square")},
                {"eyes", new SType("eyes")},
                {"eye", new SType("eye")},
                {"eyesAndMouth", new SType("eyesAndMouth")},
                {"head", new SType("head")},
            };
        }

        public SObjects GetGrammar()
        {
            return GetElement(startElementType);
        }
        private SObjects GetElement(SType elementType)
        {
            if (elementType is TypeOfObjects terminalElementType) return terminalElementType.Default;
            Rule rule = rules.FirstOrDefault(x => x.StartElementType.Name == elementType.Name);
            return rule.TransformConnect(GetElement(rule.FirstArgumentType), GetElement(rule.SecondArgumentType));
        }

        public RecognazingResult IsAtGrammar(IEnumerable<SObjects> baseElements)
        {
            var elements = new ConcurrentBag<SObjects>(baseElements);
            for (int i = 0; i < rules.Count; i++)
            {
                ContainRuleAgrumentsResult result = ContainRuleAgruments(elements, rules[i]);
                elements = result.Elements;
                if (!result.IsElementFound)
                    return new RecognazingResult(rules[i].StartElementType.Name, false);
            }
            return new RecognazingResult("", true);
        }

        private static ContainRuleAgrumentsResult ContainRuleAgruments(ConcurrentBag<SObjects> elements, Rule rule)
        {
            var result = new ContainRuleAgrumentsResult
            {
                Elements = new ConcurrentBag<SObjects>(elements),
                IsElementFound = false
            };

            foreach (SObjects firstElement in elements)
            {
                if (firstElement.Type.Name == rule.FirstArgumentType.Name)
                {
                    result = ContainRuleAgrumentsForFirstElement(elements, rule, firstElement, result);
                }
            }

            return result;
        }
        private static ContainRuleAgrumentsResult ContainRuleAgrumentsForFirstElement(IEnumerable<SObjects> elements, Rule rule, SObjects firstElement, ContainRuleAgrumentsResult result)
        {
            SObjects element = firstElement;
            Parallel.ForEach(elements, (SObjects secondElement) =>
            {
                if (rule.IsRulePare(element, secondElement))
                {
                    result.Elements.Add(rule.Connect(element, secondElement));
                    result.IsElementFound = true;
                }
            });

            return result;
        }

        public static SObjects GetTerminalElement(SLine line)
        {
            String resultName = GetTerminalElementName(line);
            return new SObjects(ElementTypes[resultName], line);
        }

        private static string GetTerminalElementName(SLine line)
        {
            double deltaX = line.Start.X - line.Finish.X;
            double deltaY = line.Start.Y - line.Finish.Y;

            if (Math.Abs(deltaY) < 1) return "a1";
            if (Math.Abs(deltaX) < 1) return "a2";
            if (Math.Abs(deltaX) < 1) return "a2";
            if (Math.Abs(deltaX / deltaY) < 0.2) return "a2";
            if (Math.Abs(deltaY / deltaX) < 0.2) return "a1";

            Point highPoint;
            if (line.Finish.Y > line.Start.Y) highPoint = line.Finish;
            else highPoint = line.Start;
            Point lowPoint;
            if (line.Finish.Y < line.Start.Y) lowPoint = line.Finish;
            else lowPoint = line.Start;
            if (highPoint.X < lowPoint.X) return "a4";

            return "a3";
        }
    }
}
