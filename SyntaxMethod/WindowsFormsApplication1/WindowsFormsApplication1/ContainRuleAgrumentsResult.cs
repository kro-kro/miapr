﻿using System.Collections.Concurrent;

namespace WindowsFormsApplication1
{
    public class ContainRuleAgrumentsResult
    {
        public ConcurrentBag<SObjects> Elements { get; set; }
        public bool IsElementFound { get; set; }
    }
}
