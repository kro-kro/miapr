﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApplication1
{
    public class UpRule : Rule
    {
        private const int randomDelta = 3;

        public UpRule(SType startElementType, SType firstArgumentType, SType secondArgumentType)
            : base(startElementType, firstArgumentType, secondArgumentType) { }

        public override SObjects TransformConnect(SObjects first, SObjects second)
        {
            MakeSameLength(first, second);
            first.Shift(0, second.StartPosition.Y + Random.Next(0, 3));

            return Connect(first, second);
        }

        public override SObjects Connect(SObjects first, SObjects second)
        {
            var resultLines = new List<SLine>(first.Lines);

            resultLines.AddRange(second.Lines);

            var connect = new SObjects(StartElementType, resultLines, first.StartPosition,
                second.EndPosition);

            return connect;
        }
        private static void MakeSameLength(SObjects first, SObjects second)
        {
            SObjects largestElement = GetLargestElement(first, second);
            SObjects shortestElement = GetShortestElement(first, second);

            shortestElement.Scale(largestElement.Length / shortestElement.Length, 1.0);
        }
        private static SObjects GetLargestElement(SObjects first, SObjects second)
        {
            if (first.Length > second.Length)
                return first;
            else
                return second;
        }
        private static SObjects GetShortestElement(SObjects first, SObjects second)
        {
            if (first.Length < second.Length)
                return first;
            else
                return second;
        }
        public override bool IsRulePare(SObjects first, SObjects second)
        {
            if (first.Type.Name != FirstArgumentType.Name ||
                second.Type.Name != SecondArgumentType.Name)
                return false;

            return second.StartPosition.Y - randomDelta < first.EndPosition.Y;
        }
    }
}
