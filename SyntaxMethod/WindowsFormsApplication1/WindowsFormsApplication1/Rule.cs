﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public abstract class Rule
    {
        protected Rule(SType startElementType, SType firstArgumentType, SType secondArgumentType)
        {
            SecondArgumentType = secondArgumentType;
            FirstArgumentType = firstArgumentType;
            StartElementType = startElementType;
            Random = new Random();
        }

        protected Random Random { get; private set; }
        public SType StartElementType { get; private set; }
        public SType FirstArgumentType { get; private set; }
        public SType SecondArgumentType { get; private set; }

        public abstract SObjects TransformConnect(SObjects first, SObjects second);
        public abstract SObjects Connect(SObjects first, SObjects second);
        public abstract bool IsRulePare(SObjects first, SObjects second);
    }
}
