﻿using System;

namespace WindowsFormsApplication1
{
    public class SType
    {
        public SType(string name) { Name = name; }
        public String Name { get; private set; }
    }
}
