﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApplication1
{
    public class InsideRule : Rule
    {
        private const int randomDelta = 3;

        public InsideRule(SType startElementType, SType firstArgumentType,
            SType secondArgumentType) : base(startElementType, firstArgumentType, secondArgumentType) { }

        public override SObjects TransformConnect(SObjects first, SObjects second)
        {
            second.Scale(first.Length / second.Length + 0.8, first.Height / second.Height + 0.8);
            first.Shift(
                second.StartPosition.X +
                Random.Next((int)(Math.Abs(first.Length - second.Length) * 0.5), (int)(Math.Abs(first.Length - second.Length) * 0.8)) -
                first.StartPosition.X,
                second.EndPosition.Y +
                Random.Next((int)(Math.Abs(first.Height - second.Height) * 0.5), (int)(Math.Abs(first.Height - second.Height) * 0.8)) -
                first.EndPosition.Y
            );

            return Connect(first, second);
        }

        public override SObjects Connect(SObjects first, SObjects second)
        {
            var resultLines = new List<SLine>(first.Lines);

            resultLines.AddRange(second.Lines);

            var connect = new SObjects(StartElementType, resultLines, second.StartPosition,
                second.EndPosition);

            return connect;
        }

        public override bool IsRulePare(SObjects first, SObjects second)
        {
            if (first.Type.Name != FirstArgumentType.Name ||
                second.Type.Name != SecondArgumentType.Name)
                return false;

            return first.StartPosition.X > second.StartPosition.X - randomDelta &&
                   first.StartPosition.Y - randomDelta < second.StartPosition.Y
                   && first.EndPosition.X - randomDelta < second.EndPosition.X &&
                   first.EndPosition.Y > second.EndPosition.Y - randomDelta;
        }
    }
}
