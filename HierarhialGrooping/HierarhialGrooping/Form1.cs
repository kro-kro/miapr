﻿using System;
using System.Windows.Forms;

namespace HierarhialGrooping
{
    public partial class Form1 : Form
    {
        private double[,] distances;

        public Form1()
        {
            InitializeComponent();
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            distances = RandomGrid((int)objectCountInput.Value);
            var hierarchical = new HierarhicalGrouping(distances, (int)objectCountInput.Value);
            hierarchical.FindGroups();
            if (radioButtonMin.Checked)
            {
                hierarchical.Draw(resultDraw);
            }
            else
            {
                hierarchical.Draw(resultDraw);
            }
        }

        private double[,] RandomGrid(int size)
        {
            resultDataGrid.ColumnCount = size;
            resultDataGrid.RowCount = size;

            var result = new double[size, size];
            var rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                result[i, i] = 0;
            }
            for (int i = 1; i < size; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    result[i, j] = rnd.Next(40) + 1;
                    result[j, i] = result[i, j];
                }
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    resultDataGrid[i, j].Value = result[i, j];
                }
            }
            if (radioButtonMax.Checked)
            {
                for (int i = 1; i < (int)objectCountInput.Value; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        result[i, j] = 50 - result[i, j];
                        result[j, i] = result[i, j];
                    }
                }
            }
            return result;
        }
    }
}
