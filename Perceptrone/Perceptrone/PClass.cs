﻿using System.Collections.Generic;

namespace Perceptrone
{
    internal class PClass
    {
        public List<PObject> objectCollection = new List<PObject>();

        public PClass() { }

        public PClass(List<PObject> objectCollection)
        {
            this.objectCollection = objectCollection;
        }
    }
}
