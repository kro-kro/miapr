﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Perceptrone
{
    internal class Percaptone
    {
        private int classCount;
        private int objectCount;
        private int attributeCount;
        private List<PClass> classList = new List<PClass>();
        private List<PObject> functionList = new List<PObject>();
        private List<PObject> weigthList = new List<PObject>();
        private List<int> decisionList = new List<int>();

        private const int MAX_RAND = 20;

        public Percaptone(int classCount, int objectCount, int attributeCount)
        {
            this.classCount = classCount;
            this.objectCount = objectCount;
            this.attributeCount = attributeCount;
        }

        internal void Show(ListBox classStructView, ListBox functionListView)
        {
            classStructView.Items.Clear();
            functionListView.Items.Clear();
            int curr = 1;
            foreach (PClass currClass in classList)
            {
                classStructView.Items.Add(String.Format("Class {0}:", curr));
                int i = 1;
                foreach (PObject currObj in currClass.objectCollection)
                {
                    string str = String.Format("    Object {0}: (", i);
                    for (int j = 0; j < currObj.attributeCollection.Count - 1; j++)
                    {
                        int attribute = currObj.attributeCollection[j];
                        str += attribute + "; ";
                    }
                    str = str.Remove(str.Length - 2, 2);
                    str += ")";
                    classStructView.Items.Add(str);
                    i++;
                }
                classStructView.Items.Add("");
                curr++;
            }
            functionListView.Items.Add("Functions: ");
            for (int i = 0; i < functionList.Count; i++)
            {
                string str = $"d{i + 1}(X): ";
                for (int j = 0; j < functionList[i].attributeCollection.Count; j++)
                {
                    int attribute = functionList[i].attributeCollection[j];
                    if (j < functionList[i].attributeCollection.Count - 1)
                    {
                        if (attribute >= 0 && j != 0) str += "+";
                        str += $"{attribute}*X{j + 1}";
                    }
                    else
                    {
                        if (attribute >= 0 && j != 0) str += "+";
                        str += attribute;
                    }
                }
                functionListView.Items.Add(str);
            }
        }

        internal object FindClass(PObject pObject)
        {
            int result = 0;
            pObject.attributeCollection.Add(1);
            int dMax = MultiplicatePObj(weigthList[0], pObject);
            for (int i = 1; i < weigthList.Count; i++)
            {
                PObject weigth = weigthList[i];
                if (MultiplicatePObj(weigth, pObject) > dMax)
                {
                    dMax = MultiplicatePObj(weigth, pObject);
                    result = i;
                }
            }
            return result + 1;
        }

        public void Calculate()
        {
            CreateClasses();
            GenerateFunctions();
            if (!IsDone())
            {
                if (MessageBox.Show("Wrong functions!", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.Cancel)
                {
                    Application.Exit();
                }
            }
        }

        private bool IsDone()
        {
            for (int i = 0; i < classList.Count; i++)
            {
                foreach (PObject currObject in classList[i].objectCollection)
                {
                    int objDec = MultiplicatePObj(weigthList[i], currObject);
                    int maxDec = objDec;
                    Parallel.For(0, functionList.Count(), j =>
                    {
                        if (MultiplicatePObj(functionList[i], currObject) > maxDec)
                        {
                            maxDec = MultiplicatePObj(functionList[i], currObject);
                        }
                    });
                    if (maxDec > objDec)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void GenerateFunctions()
        {
            bool continueCalc = true;
            for (int i = 0; i < 10000; i++)
            {
                if (!continueCalc) break;
                for (int j = 0; j < classList.Count; j++)
                {
                    PClass currClass = classList[j];
                    for (int k = 0; k < currClass.objectCollection.Count; k++)
                    {
                        continueCalc = NeedNextCalculation(currClass, currClass.objectCollection[k], weigthList[j], j);
                    }
                }
            }
            if (continueCalc)
            {
                MessageBox.Show("More than 10000 iterations.");
            }
            functionList = weigthList;
        }

        private bool NeedNextCalculation(PClass pClass, PObject pObject, PObject weigth, int classNumber)
        {
            bool result = false;
            int objDec = MultiplicatePObj(weigth, pObject);
            for (int i = 0; i < weigthList.Count; i++)
            {
                decisionList[i] = MultiplicatePObj(weigthList[i], pObject);
                if (i != classNumber)
                {
                    if (objDec <= decisionList[i])
                    {
                        ChangeWeigth(weigthList[i], pObject, -1);
                        result = true;
                    }
                }
            }
            if (result)
            {
                ChangeWeigth(weigth, pObject, 1);
            }
            return result;
        }

        private void ChangeWeigth(PObject weigth, PObject pObject, int sign)
        {
            sign = sign / Math.Abs(sign);
            Parallel.For(0, weigth.attributeCollection.Count, i =>
            {
                weigth.attributeCollection[i] += (sign) * pObject.attributeCollection[i];
            });
        }

        private int MultiplicatePObj(PObject pObject1, PObject pObject2)
        {
            int result = 0;
            for (int i = 0; i < pObject1.attributeCollection.Count; i++)
            {
                result += pObject1.attributeCollection[i] * pObject2.attributeCollection[i];
            }
            return result;
        }

        private void CreateClasses()
        {
            Random rnd = new Random();
            Parallel.For(0, classCount, i =>
            {
                PClass newClass = new PClass();
                for (int j = 0; j < objectCount; j++)
                {
                    PObject newObject = new PObject();
                    for (int k = 0; k < attributeCount; k++)
                    {
                        newObject.attributeCollection.Add(rnd.Next(MAX_RAND) - MAX_RAND / 2);
                    }
                    newClass.objectCollection.Add(newObject);
                }
                classList.Add(newClass);
            });
            for (int i = 0; i < classList.Count; i++)
            {
                PObject weigth = new PObject();
                for (int j = 0; j <= attributeCount; j++)
                {
                    weigth.attributeCollection.Add(0);
                }
                weigthList.Add(weigth);
                decisionList.Add(0);
            }
            AdditionObjects(classList);
        }

        private void AdditionObjects(List<PClass> classList)
        {
            Parallel.ForEach(classList, pClass =>
            {
                foreach (PObject pObject in pClass.objectCollection)
                {
                    pObject.attributeCollection.Add(1);
                }
            });
        }
    }
}
