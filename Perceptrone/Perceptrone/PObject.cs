﻿using System.Collections.Generic;

namespace Perceptrone
{
    internal class PObject
    {
        public List<int> attributeCollection = new List<int>();

        public PObject() { }

        public PObject(List<int> attributeCollection)
        {
            this.attributeCollection = attributeCollection;
        }
    }
}
