﻿using System;
using System.Windows.Forms;

namespace Perceptrone
{
    public partial class Form1 : Form
    {
        private Percaptone pCalculator;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            var perceptron = new Percaptone((int)classCount.Value,
                (int)objectCount.Value, (int)attributeCount.Value);
            perceptron.Calculate();
            perceptron.Show(classStruct, functionList);
            pCalculator = perceptron;
        }

        private void buttonExamine_Click(object sender, EventArgs e)
        {
            PObject examObj = new PObject();
            string[] numsStr = textTest.Text.Split(' ');
            int[] nums = new int[numsStr.Length];
            try
            {
                for (int i = 0; i < numsStr.Length; i++)
                {
                    nums[i] = Convert.ToInt32(numsStr[i]);
                }
                if (nums.Length == attributeCount.Value)
                {
                    for (int i = 0; i < nums.Length; i++)
                    {
                        examObj.attributeCollection.Add(nums[i]);
                    }
                }
                else
                {
                    MessageBox.Show("Incorrect input.");
                }
                MessageBox.Show(String.Format("Pojects from class #{0}", pCalculator.FindClass(examObj)));
            }
            catch (Exception)
            {
                MessageBox.Show("Incorrect input. Must be count of nums devided by space.");
            }
        }
    }
}
