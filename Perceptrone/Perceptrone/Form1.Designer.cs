﻿namespace Perceptrone
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.classCount = new System.Windows.Forms.NumericUpDown();
            this.objectCount = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.attributeCount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.classStruct = new System.Windows.Forms.ListBox();
            this.functionList = new System.Windows.Forms.ListBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.textTest = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonExamine = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.classCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeCount)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class count";
            // 
            // classCount
            // 
            this.classCount.Location = new System.Drawing.Point(12, 85);
            this.classCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.classCount.Name = "classCount";
            this.classCount.Size = new System.Drawing.Size(112, 22);
            this.classCount.TabIndex = 1;
            this.classCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // objectCount
            // 
            this.objectCount.Location = new System.Drawing.Point(12, 130);
            this.objectCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.objectCount.Name = "objectCount";
            this.objectCount.Size = new System.Drawing.Size(112, 22);
            this.objectCount.TabIndex = 3;
            this.objectCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Objectss in class";
            // 
            // attributeCount
            // 
            this.attributeCount.Location = new System.Drawing.Point(12, 175);
            this.attributeCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.attributeCount.Name = "attributeCount";
            this.attributeCount.Size = new System.Drawing.Size(112, 22);
            this.attributeCount.TabIndex = 5;
            this.attributeCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Object attributes";
            // 
            // classStruct
            // 
            this.classStruct.FormattingEnabled = true;
            this.classStruct.ItemHeight = 16;
            this.classStruct.Location = new System.Drawing.Point(130, 12);
            this.classStruct.Name = "classStruct";
            this.classStruct.Size = new System.Drawing.Size(256, 484);
            this.classStruct.TabIndex = 6;
            // 
            // functionList
            // 
            this.functionList.FormattingEnabled = true;
            this.functionList.ItemHeight = 16;
            this.functionList.Location = new System.Drawing.Point(392, 12);
            this.functionList.Name = "functionList";
            this.functionList.Size = new System.Drawing.Size(322, 484);
            this.functionList.TabIndex = 7;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(12, 203);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(112, 34);
            this.buttonCalculate.TabIndex = 8;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // textTest
            // 
            this.textTest.Location = new System.Drawing.Point(12, 303);
            this.textTest.Name = "textTest";
            this.textTest.Size = new System.Drawing.Size(112, 22);
            this.textTest.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 283);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Test";
            // 
            // buttonExamine
            // 
            this.buttonExamine.Location = new System.Drawing.Point(12, 331);
            this.buttonExamine.Name = "buttonExamine";
            this.buttonExamine.Size = new System.Drawing.Size(112, 32);
            this.buttonExamine.TabIndex = 11;
            this.buttonExamine.Text = "Examine";
            this.buttonExamine.UseVisualStyleBackColor = true;
            this.buttonExamine.Click += new System.EventHandler(this.buttonExamine_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 505);
            this.Controls.Add(this.buttonExamine);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textTest);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.functionList);
            this.Controls.Add(this.classStruct);
            this.Controls.Add(this.attributeCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.objectCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.classCount);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.classCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown classCount;
        private System.Windows.Forms.NumericUpDown objectCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown attributeCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox classStruct;
        private System.Windows.Forms.ListBox functionList;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TextBox textTest;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonExamine;
    }
}

